package com.jeucarte;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;


public class Plateau {
    private ArrayList<Joueur> joueurs = new ArrayList<>();
    private ArrayList<Carte> pioche = new ArrayList<>();
    private List<Carte> cartesJouer = new ArrayList<>();
    private Joueur premier;

    public Plateau() throws IOException {
        chargePioche();
        ordreJoueur();
        distribution();
        jeu();
    }

    public void chargePioche() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader("src/com/jeucarte/data/timeline/timeline.csv"));
        String line = "";
        while ((line = bufferedReader.readLine()) != null) {
            String[] mot = line.split(";");
            Carte carte = new Carte(mot[0], strToInt(mot[1]), mot[2]);
            pioche.add(carte);
        }
        pioche.remove(0);
        Collections.shuffle(pioche);
    }

    public static int strToInt(String annee) {
        int i = 0;
        int num = 0;
        boolean isNeg = false;

        if (annee.charAt(0) == '-') {
            isNeg = true;
            i = 1;
        }

        while (i < annee.length()) {
            num *= 10;
            num += annee.charAt(i++) - '0';
        }

        if (isNeg)
            num = -num;
        return num;
    }

    public void piocheCarte(Joueur joueur){
        joueur.getMainJoueur().add(pioche.get(0));
        pioche.remove(0);
    }

    public void ordreJoueur() {
        joueurs.add(new Joueur("joueur1", 25));
        joueurs.add(new Joueur("joueur2", 18));
        joueurs.add(new Joueur("joueur3", 45));

        ArrayList<Joueur> trier = new ArrayList<>();
        trier.addAll((Collection<? extends Joueur>) joueurs.clone());
        trier.sort(Comparator.comparing(Joueur::getAgeJoueur).thenComparing(Joueur::getNomJoueur));
        premier = trier.get(0);
        ArrayList<Joueur> joueursFinal =  new ArrayList<>();
        joueursFinal.add(premier);
        for (int i = joueurs.indexOf(premier)+1; i < joueurs.size() ; i++) {
            joueursFinal.add(joueurs.get(i));
        }
        for (int i = 0; i < joueurs.indexOf(premier) ; i++) {
            joueursFinal.add(joueurs.get(i));
        }
        joueurs.clear();
        joueurs.addAll(joueursFinal);

    }

    public void distribution() {
        if (joueurs.size() == 2 || joueurs.size() == 3) {
            for (Joueur joueur : joueurs) {
                for (int j = 0; j < 3; j++) {
                    piocheCarte(joueur);
                }
                //System.out.println(joueur.getMainJoueur().toString());
            }
        } else if (joueurs.size() == 4 || joueurs.size() == 5) {
            for (Joueur joueur : joueurs) {
                for (int j = 0; j < 5; j++) {
                    piocheCarte(joueur);
                }
                //System.out.println(joueur.getMainJoueur().size());
            }
        } else if (joueurs.size() >= 6 && joueurs.size() <= 8) {
            for (Joueur joueur : joueurs) {
                for (int j = 0; j < 4; j++) {
                    piocheCarte(joueur);
                }
                //System.out.println(joueur.getMainJoueur().size());
            }
        }
    }

    public void jeu() {
        cartesJouer.add(pioche.get(0));
        pioche.remove(0);
        System.out.println("Le plus jeune commence : " + premier.getNomJoueur());

        boolean partieFini=false;
        Joueur vainqueur = new Joueur();
        ArrayList<Joueur> resteVainqueur = new ArrayList<>();
        int tour = 1;

        while (partieFini==false) {

            System.out.println("Nous sommes au tour : "+ tour);
            for (int j = 0; j < joueurs.size(); j++) {

                System.out.println("Voici les cartes posé : ");
                for (Carte carte : cartesJouer){
                    System.out.print(carte.getAnneeCarte() +" | ");
                }

                System.out.println("\n"+joueurs.get(j).getNomJoueur() +" a vous de joué!");
                System.out.println("Voici votre main ");
                for (Carte carte : joueurs.get(j).getMainJoueur()){
                    System.out.print(carte.getAnneeCarte()+" | ");
                }

                Scanner poseCarte = new Scanner(System.in);
                System.out.println("\nQuelle carte voulez vous joué (donnez le numéro) ? \n");
                int pose = poseCarte.nextInt();
                Carte cartePose = joueurs.get(j).getMainJoueur().get(pose);
                int indexCartePose = joueurs.get(j).getMainJoueur().indexOf(cartePose);

                Scanner choixCarte = new Scanner(System.in);
                System.out.println("A coté de quelle carte voulez vous joué (donnez le numéro) ? \n");
                int choixPose = choixCarte.nextInt();

                Scanner endroit = new Scanner(System.in);
                System.out.println("Ou souhaitez vous la poser gauche/droite (G/D) ? \n");
                String place = endroit.nextLine();

                if (place.matches("G")) {
                    if (cartesJouer.size() == 1) {
                        if (joueurs.get(j).getMainJoueur().get(indexCartePose).getAnneeCarte() <= cartesJouer.get(choixPose).getAnneeCarte()) {
                            System.out.println("BOOOOOOON");
                            cartesJouer.add(choixPose, cartePose);
                            joueurs.get(j).getMainJoueur().remove(cartePose);
                        } else {
                            System.out.println("PASBOOON");
                            joueurs.get(j).getMainJoueur().remove(cartePose);
                            piocheCarte(joueurs.get(j));
                        }
                    } else {
                        if (choixPose == 0) {
                            if (joueurs.get(j).getMainJoueur().get(indexCartePose).getAnneeCarte() <= cartesJouer.get(choixPose).getAnneeCarte()) {
                                System.out.println("BOOOOOOON");
                                cartesJouer.add(choixPose, cartePose);
                                joueurs.get(j).getMainJoueur().remove(cartePose);
                            } else {
                                System.out.println("PASBOOON");
                                joueurs.get(j).getMainJoueur().remove(cartePose);
                                piocheCarte(joueurs.get(j));
                            }
                        } else if (cartesJouer.get(choixPose).getAnneeCarte() >= joueurs.get(j).getMainJoueur().get(indexCartePose).getAnneeCarte()
                                && cartesJouer.get(choixPose-1).getAnneeCarte() <= joueurs.get(j).getMainJoueur().get(indexCartePose).getAnneeCarte()) {
                            System.out.println("BOOOON");
                            cartesJouer.add(choixPose, cartePose);
                            joueurs.get(j).getMainJoueur().remove(cartePose);
                        } else {
                            System.out.println("PASBOOOOON");
                            joueurs.get(j).getMainJoueur().remove(cartePose);
                            piocheCarte(joueurs.get(j));
                        }
                    }
                }

                if (place.matches("D")) {
                    if (cartesJouer.size() == 1) {
                        if (joueurs.get(j).getMainJoueur().get(indexCartePose).getAnneeCarte() >= cartesJouer.get(choixPose).getAnneeCarte()){
                            System.out.println("BOOOOON");
                            cartesJouer.add(choixPose + 1, cartePose);
                            joueurs.get(j).getMainJoueur().remove(cartePose);
                        } else {
                            System.out.println("PASBOOON");
                            joueurs.get(j).getMainJoueur().remove(cartePose);
                            piocheCarte(joueurs.get(j));
                        }
                    } else {
                        if (choixPose+1 == cartesJouer.size()) {
                            if (joueurs.get(j).getMainJoueur().get(indexCartePose).getAnneeCarte() >= cartesJouer.get(choixPose).getAnneeCarte()) {
                                System.out.println("BOOOOON");
                                cartesJouer.add(choixPose + 1, cartePose);
                                joueurs.get(j).getMainJoueur().remove(cartePose);
                            } else {
                                System.out.println("PASBOOON");
                                joueurs.get(j).getMainJoueur().remove(cartePose);
                                piocheCarte(joueurs.get(j));
                            }
                        } else if (cartesJouer.get(choixPose).getAnneeCarte() <= joueurs.get(j).getMainJoueur().get(indexCartePose).getAnneeCarte()
                                && cartesJouer.get(choixPose+1).getAnneeCarte() >= joueurs.get(j).getMainJoueur().get(indexCartePose).getAnneeCarte()) {
                            System.out.println("BOOOOON");
                            cartesJouer.add(choixPose + 1, cartePose);
                            joueurs.get(j).getMainJoueur().remove(cartePose);

                        } else {
                            System.out.println("PASBOOOOOON");
                            joueurs.get(j).getMainJoueur().remove(cartePose);
                            piocheCarte(joueurs.get(j));
                        }
                    }
                }
                if (joueurs.get(j).getMainJoueur().isEmpty()){
                    resteVainqueur.add(joueurs.get(j));
                }
            }
            if (resteVainqueur.size() == 1){
                vainqueur = resteVainqueur.get(0);
                partieFini=true;
            } else if (resteVainqueur.size() >=2){
                joueurs.clear();
                joueurs.addAll(resteVainqueur);
                resteVainqueur.clear();
                for (Joueur joueur : joueurs){
                    piocheCarte(joueur);
                }
            }


            tour ++;
        }
        System.out.println("Le vainqueur est : "+ vainqueur.getNomJoueur());

    }


}
package com.jeucarte;

import java.util.ArrayList;

public class Joueur {
    private String nomJoueur;
    private int ageJoueur;
    private ArrayList<Carte> mainJoueur = new ArrayList<>();

    public Joueur(String nomJoueur, int ageJoueur) {
        this.nomJoueur = nomJoueur;
        this.ageJoueur = ageJoueur;
    }

    public Joueur(){}

    public String getNomJoueur() {
        return nomJoueur;
    }

    public int getAgeJoueur() {
        return ageJoueur;
    }

    public ArrayList<Carte> getMainJoueur() {
        return mainJoueur;
    }

    @Override
    public String toString() {
        return "Joueur{" +
                "nomJoueur='" + nomJoueur + '\'' +
                ", ageJoueur=" + ageJoueur +
                '}';
    }
}

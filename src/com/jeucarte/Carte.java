package com.jeucarte;

public class Carte {
    private String nomCarte;
    private Integer anneeCarte;
    private String coteImageCarte;
    private String coteDateCarte;


    public Carte(String nomCarte, Integer anneeCarte, String coteImageCarte) {
        this.nomCarte = nomCarte;
        this.anneeCarte = anneeCarte;
        this.coteImageCarte = coteImageCarte;
    }

    public String getNomCarte() {
        return nomCarte;
    }

    public void setNomCarte(String nomCarte) {
        this.nomCarte = nomCarte;
    }

    public Integer getAnneeCarte() {
        return anneeCarte;
    }

    public void setAnneeCarte(Integer anneeCarte) {
        this.anneeCarte = anneeCarte;
    }

    public String getCoteImageCarte() {
        return coteImageCarte;
    }

    public void setCoteImageCarte(String coteImageCarte) {
        this.coteImageCarte = coteImageCarte;
    }

    public String getCoteDateCarte() {
        return coteDateCarte;
    }

    public void setCoteDateCarte(String coteDateCarte) {
        this.coteDateCarte = coteDateCarte;
    }

    @Override
    public String toString() {
        return nomCarte;
    }
}
